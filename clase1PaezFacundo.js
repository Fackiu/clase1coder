let dato = prompt('Ingrese un dato')

if(parseInt(dato) > 1000) {
    alert(`Variable dato (${dato}) es mayor o igual que 1000.`)
} else if (dato == 'Hola') {
    alert(`El mensaje dice ${dato}`)
} else if(parseInt(dato) > 10 && dato < 50){
    alert(`Variable dato (${dato}) esta entre 10 y 50`)
} else {
    alert('No se cumplen ninguna de las condiciones anteriores')
}